#pragma once//Daniel samra
#include "OutStream.h"
class OutStreamEncrypted: public OutStream
{
private:
	int heist;
public:
	OutStreamEncrypted(int heist);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)());
};

