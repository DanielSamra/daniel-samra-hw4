#pragma once
//Daniel samra
#define _CRT_SECURE_NO_WARNINGS
#include <string>
class OutStream
{
protected:
	FILE * _fp;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
};

void endline();