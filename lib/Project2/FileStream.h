#pragma once//Daniel samra
#include "OutStream.h"
namespace msl
{
	class FileStream : public OutStream
	{
	public:
		FileStream(const char* path);
		~FileStream();
		FileStream& operator<<(const char *str);
		FileStream& operator<<(int num);
		FileStream& operator<<(void(*pf)());
		void endline();
	};
}
