#pragma once//Daniel samra
#include "OutStream.h"
#include <stdio.h>

class Logger
{
	OutStream os;
	static unsigned int num;
	bool _startLine;
	void setStartLine();
public:
	Logger();
	~Logger();
	bool Get_SartLine();
	friend Logger& operator<<(Logger& l, const char *msg);
	friend Logger& operator<<(Logger& l, int num);
	friend Logger& operator<<(Logger& l, void(*pf)());
};
