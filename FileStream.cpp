#include "FileStream.h"//Daniel samra
#include <string>


FileStream::FileStream(const char* path)
{
	_fp = fopen(path, "w");
}


FileStream::~FileStream()
{
	fclose(_fp);
}
FileStream& FileStream::operator<<(const char *str)
{
	fprintf(_fp,"%s", str);
	return *this;
}

FileStream& FileStream::operator<<(int num)
{
	fprintf(_fp,"%d", num);
	return *this;
}

FileStream& FileStream::operator<<(void(*pf)())
{
	pf();
	return *this;
}


void FileStream::endline()
{
	fprintf(_fp,"%s","\n");
}

